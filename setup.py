from setuptools import setup

setup(
    name='dhsocketserver',
    version='0.0.1',
    description='My private package from private github repo',
    url='https://runepoulsen@bitbucket.org/runepoulsen/dhsocketserver.git',
    author='Rune',
    author_email='runesp@gmail.com',
    license='unlicense',
    packages=['dhsocketserver'],
    zip_safe=False
)