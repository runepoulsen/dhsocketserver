import eventlet

eventlet.monkey_patch()
import socketio
from eventlet import wsgi


class DhSocketServer:

    def __init__(self, port, name=None, binary=True, debug=False):
        self.name = name
        self.sio = None
        self.debug = debug
        self.port = int(port)
        self.clients = []
        self.room_clients = {}
        self.sio = socketio.Server(async_mode='eventlet', cors_allowed_origins='*', binary=binary)
        self.sio.on('connect', self.on_connect)
        self.sio.on('disconnect', self.on_disconnect)
        self.sio.on('enter_room', self.enter_room)
        self.sio.on('leave_room', self.leave_room)
        self.app = socketio.WSGIApp(self.sio)
        self.server_thread = None

    def emit(self, payload, cmd="message", room=None):
        self.sio.emit(cmd, payload, room=room)
        eventlet.sleep(0)

    def n_clients(self, room=None):
        if not room: return len(self.clients)
        if room in self.room_clients: return len(self.room_clients[room])
        return 0

    def server_process(self):
        eventlet.wsgi.server(eventlet.listen(('', self.port)), self.app, log_output=False)

    def start(self):
        if self.debug:
            print(f"{self.name} starting on port {self.port}")
        self.server_thread = eventlet.spawn(self.server_process)

    def on_connect(self, sid, environ):
        if sid not in self.clients: self.clients.append(sid)
        self.print_status()
        self.on_event(sid=sid)

    def on_disconnect(self, sid):
        if sid in self.clients: self.clients.remove(sid)
        for room, clients in self.room_clients.items():
            if sid in clients: clients.remove(sid)
        self.print_status()
        self.on_event(sid=sid)

    def print_status(self):
        if not self.debug: return
        print(f"SERVER {self.name}, clients: %s" % (len(self.clients)))

    def close(self):
        self.server_thread.kill()

    def enter_room(self, sid, room):
        if room not in self.room_clients: self.room_clients[room] = []
        room_clients = self.room_clients[room]
        if sid not in room_clients: room_clients.append(sid)
        self.sio.enter_room(sid, room)
        self.print_status()
        self.on_event(sid=sid, room=room)

    def leave_room(self, sid, room):
        if room not in self.room_clients: self.room_clients[room] = []
        room_clients = self.room_clients[room]
        if sid in room_clients: room_clients.remove(sid)
        self.sio.leave_room(sid, room)
        self.print_status()
        self.on_event(sid=sid, room=room)

    def on_event(self, sid=None, room=None):
        pass
